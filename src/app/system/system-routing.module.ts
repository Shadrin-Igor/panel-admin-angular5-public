import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {SystemComponent} from './system.component';
import {DashboardComponent} from './dashboard/dashboard.component';

const routes: Routes = [
  {
    path: '',
    component: SystemComponent,
    data: {
      breadcrumb: ""
    },
    children: [
    {path: '', component: DashboardComponent},
    {
      path: 'firms',
      data: {
        breadcrumb: "Мои фирмы"
      },
      loadChildren: 'app/system/firms/firms.module#FirmsModule'
    }
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SystemRoutingModule {
}
