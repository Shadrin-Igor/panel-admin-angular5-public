import {Component, OnDestroy, OnInit} from '@angular/core';

import {FirmService} from '../../shared/services/firm.service';
import {Subscription} from 'rxjs/Subscription';
import {ToasterService} from 'angular2-toaster';
import HelperService from '../../shared/services/helper.service';
import {Router} from '@angular/router';

@Component({
  selector: 'wtp-firms',
  templateUrl: './firms.component.html',
  styleUrls: ['./firms.component.scss']
})
export class FirmsComponent implements OnInit, OnDestroy {

  settings: {} = {};
  listFirms: any[] = [];
  subs: Subscription[] = [];
  isLoaded: boolean = false;
  translates = {
    FIRM_SUCCESS_DELETED: 'Фирма успешно удалена',
    STATUS_NOT_PUBLISH: 'Не опубликована',
    STATUS_MODERATION: 'На модерации',
    STATUS_PUBLISH: 'Опубликована'
  };

  constructor(private firmService: FirmService,
              private helperService: HelperService,
              private router: Router,
              private toasterService: ToasterService) {
  }

  ngOnInit() {
    this.settings = {
      noDataMessage: 'У вашей компании еще нет добавленных фирм',
      gallery: 'images',
      columns: [{
        field: 'id',
        title: 'ID',
        type: 'number',
        width: 50
      },
        {
          field: 'name',
          title: 'Название',
          type: 'string',
          filter: true,
          template: row => this.rowTemplate(row)
        }]
    };
  }

  rowTemplate(row: any): string {
    let cout = `<h3 class="color-green">${row.name}</h3><p>${row.description_small}</p>`;

    cout += '<div class="alert alert-hint">';
    if (row.statistic) {
      if (row.statistic.services) cout += `<strong>Услуг:</strong> ${row.statistic.services}&nbsp;&nbsp;`;
      if (row.statistic.pages) cout += `<strong>Страниц:</strong> ${row.statistic.pages}&nbsp;&nbsp;`;
    }
    cout += `<strong>Статус:</strong> ${this.getFirmStatus(row.status)}`;
    cout += '</div>';

    return cout;
  }

  getFirmStatus(status) {
    switch(+status) {
      case 1: return `<span class="text-danger">${this.translates.STATUS_NOT_PUBLISH}</span>`;
      case 2: return `<span class="text-primary">${this.translates.STATUS_MODERATION}</span>`;
      case 3: return `<span class="text-success">${this.translates.STATUS_PUBLISH}</span>`;
    }
  }

  getList(cb = null) {
    const sub = this.firmService.getAll()
      .subscribe((res: any) => {
        if (res.status === 200) {
          this.listFirms = res.data.items.map(item => ({
            ...item, description_small: this.helperService.stripTags(item.description, 400)
          }));

          this.isLoaded = true;
          if (cb) {
            cb();
          }
        }
      });
    this.subs.push(sub);
  }

  onEdit(id) {
    this.router.navigate(['/system/firms', id, 'common']);
  }

  create() {
    this.router.navigate(['/system/firms/common']);
  }

  ngOnDestroy() {
    this.helperService.unSubscribe(this.subs);
  }
}
