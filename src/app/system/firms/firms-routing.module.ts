import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {FirmCommonComponent} from './firm/firm-common/firm-common.component';
import {FirmServicesComponent} from './firm/firm-services/firm-services.component';
import {FirmConfirmComponent} from './firm/firm-confirm/firm-confirm.component';
import {FirmAboutComponent} from './firm/firm-about/firm-about.component';
import {FirmComponent} from './firm/firm.component';
import {FirmPagesComponent} from './firm/firm-pages/firm-pages.component';
import {FirmsComponent} from './firms.component';
import {FirmPublicationComponent} from './firm/firm-publication/firm-publication.component';

const router: Routes = [
  {
    path: '',
    component: FirmsComponent,
    data: {
      breadcrumb: null
    },
  },
  {
    path: ':id/:tab',
    component: FirmComponent,
    data: {
      breadcrumb: "Описание фирмы"
    },
    /*children: [
      {path: 'common', component: FirmCommonComponent},
      {
        path: ':id/common',
        data: {
          breadcrumb: "Общая информация"
        },
        component: FirmCommonComponent
      },
      {
        path: ':id/about',
        data: {
          breadcrumb: "О компании"
        },
        component: FirmAboutComponent
      },
      {
        path: ':id/services',
        data: {
          breadcrumb: "Услуги фирмы"
        },
        component: FirmServicesComponent
      },
      {
        path: ':id/pages',
        data: {
          breadcrumb: "Страницы фирмы"
        },
        component: FirmPagesComponent
      },
      {
        path: ':id/preview',
        data: {
          breadcrumb: "Просмотр фирмы"
        },
        component: FirmConfirmComponent
      },
      {
        path: ':id/publication',
        data: {
          breadcrumb: "Публикация"
        },
        component: FirmPublicationComponent
      }
    ]*/
  },
];

@NgModule({
  imports: [RouterModule.forChild(router)],
  exports: [RouterModule]
})
export class FirmsRoutingModule {

}
