import {NgModule} from '@angular/core';

import {SharedModule} from '../../shared/shared.module';
import {FirmComponent} from './firm/firm.component';
import {FirmsComponent} from './firms.component';
import {FirmsRoutingModule} from './firms-routing.module';
import {FirmCommonComponent} from './firm/firm-common/firm-common.component';
import {FirmAboutComponent} from './firm/firm-about/firm-about.component';
import {FirmServicesComponent} from './firm/firm-services/firm-services.component';
import {FirmConfirmComponent} from './firm/firm-confirm/firm-confirm.component';
import EditFormComponent from './firm/firm-services/edit-form/edit-form.component';
import {FirmPagesComponent} from './firm/firm-pages/firm-pages.component';
import EditPagesFormComponent from './firm/firm-pages/edit-form/edit-pages-form.component';
import { FirmConfirmAboutComponent } from './firm/firm-confirm/firm-confirm-about/firm-confirm-about.component';
import { FirmConfirmCommonComponent } from './firm/firm-confirm/firm-confirm-common/firm-confirm-common.component';
import { FirmConfirmServicesComponent } from './firm/firm-confirm/firm-confirm-services/firm-confirm-services.component';
import { FirmConfirmPagesComponent } from './firm/firm-confirm/firm-confirm-pages/firm-confirm-pages.component';
import {FirmPublicationComponent} from './firm/firm-publication/firm-publication.component';

@NgModule({
  imports: [

    SharedModule,
    FirmsRoutingModule
  ],
  declarations: [
    FirmComponent,
    FirmsComponent,
    FirmCommonComponent,
    FirmAboutComponent,
    FirmServicesComponent,
    FirmPagesComponent,
    FirmConfirmComponent,
    EditFormComponent,
    EditPagesFormComponent,
    FirmConfirmAboutComponent,
    FirmConfirmCommonComponent,
    FirmConfirmServicesComponent,
    FirmConfirmPagesComponent,
    FirmPublicationComponent
  ],
  exports: [
    SharedModule
  ],
  entryComponents: [
    EditFormComponent,
    EditPagesFormComponent
  ],
})
export class FirmsModule {
}
