import {Component, Input, OnInit} from '@angular/core';
import HelperService from '../../../../../shared/services/helper.service';

@Component({
  selector: 'wtp-firm-confirm-pages',
  templateUrl: './firm-confirm-pages.component.html'
})
export class FirmConfirmPagesComponent implements OnInit{
  @Input() firmData: any = [];
  @Input() data: any;

  constructor(
    private helperService : HelperService
  ) {}

  ngOnInit() {
    if(this.firmData.pages) {
      this.firmData.pages.forEach(service => {
        service.description = this.helperService.stripTags(service.description, 50);
        if(service.images){
          service.images = service.images.map(item => ({
            fullLink: !item.fullLink ? this.helperService.getImage(item, 'big') : item.fullLink
          }));
        }
      });
    }
  }
}
