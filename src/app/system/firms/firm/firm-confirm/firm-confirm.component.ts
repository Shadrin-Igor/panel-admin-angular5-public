import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'wtp-firm-confirm',
  templateUrl: './firm-confirm.component.html',
  styleUrls: ['./firm-confirm.component.scss']
})
export class FirmConfirmComponent {
  @Input() firmData: any;
  @Input() data: any;
  isLoaded = true;
}
