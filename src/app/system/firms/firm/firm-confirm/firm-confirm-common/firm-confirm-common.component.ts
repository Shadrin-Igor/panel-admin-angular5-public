import { Component, OnInit, Input } from '@angular/core';
import HelperService from '../../../../../shared/services/helper.service';

@Component({
  selector: 'wtp-firm-confirm-common',
  templateUrl: './firm-confirm-common.component.html'
})
export class FirmConfirmCommonComponent{
  @Input() firmData: {} = {};
  @Input() data: any;

  constructor(
    private helperService: HelperService
  ) {}
}
