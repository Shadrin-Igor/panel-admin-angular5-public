import {Component, Input, OnInit} from '@angular/core';
import HelperService from '../../../../../shared/services/helper.service';

@Component({
  selector: 'wtp-firm-confirm-services',
  templateUrl: './firm-confirm-services.component.html'
})
export class FirmConfirmServicesComponent implements OnInit{
  @Input() firmData: any;
  @Input() data: any;

  constructor(private helperService : HelperService) {}

  ngOnInit() {
    if(this.firmData.services) {
      this.firmData.services.forEach(service => {
        service.description = this.helperService.stripTags(service.description, 50);
        if(service.images){
          service.images = service.images.map(item => ({
            fullLink: !item.fullLink ? this.helperService.getImage(item, 'big') : item.fullLink
          }));
        }
      });
    }
  }
}
