import {Component, Input, OnInit} from '@angular/core';

import HelperService from '../../../../../shared/services/helper.service';

@Component({
  selector: 'wtp-firm-confirm-about',
  templateUrl: './firm-confirm-about.component.html',
  styleUrls: ['./firm-confirm-about.component.scss']
})
export class FirmConfirmAboutComponent implements OnInit{
  @Input() firmData: any = {};
  @Input() data: any = {};

  image: string = '';
  constructor(private helperService: HelperService) {
  }

  ngOnInit() {
    if(this.firmData.images) {
      this.image = this.helperService.getImage(this.firmData.images[0], 'big');
    }
  }
}
