import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import 'rxjs/add/operator/mergeMap';
import {ToasterService} from 'angular2-toaster';
import * as queue from 'queue';

import {DataService} from '../../../../shared/services/data.service';
import {FirmService} from '../../../../shared/services/firm.service';
import HelperService from '../../../../shared/services/helper.service';
import {EventService} from '../../../../shared/services/event.service';

@Component({
  selector: 'wtp-firm-common',
  templateUrl: './firm-common.component.html'
})
export class FirmCommonComponent implements OnInit, OnDestroy, OnChanges {
  @Input() firmData: any;
  @Input() data: any;
  @Output() changePublicStatus = new EventEmitter();
  @Output() cb = new EventEmitter<any>();

  isLoaded = false;
  isLoading = false;
  form: FormGroup;
  subLIst: Subscription[] = [];
  listCountry = [];
  listCity = [];
  errors = [];
  recommendations = [];

  translates = {
    'NETWORK_ERROR': 'NETWORK_ERROR',
    'ERROR_DATA': 'ERROR_DATA',
    'COMMON_DATA_SUCCESS_SAVE': 'COMMON_DATA_SUCCESS_SAVE',
    'VALIDATE_ERROR': 'VALIDATE_ERROR',
  };

  constructor(private dataService: DataService,
              private firmService: FirmService,
              private router: Router,
              private helperService: HelperService,
              private eventService: EventService,
              private toasterService: ToasterService) {
  }

  ngOnInit() {
    console.log('ngOnInit');
    this.form = new FormGroup({
      name: new FormControl({value: this.firmData.name, disabled: this.data.readOnly}, [Validators.required]),
      email: new FormControl({value: this.firmData.email, disabled: this.data.readOnly}, [Validators.required, Validators.email]),
      phone: new FormControl({value: this.firmData.phone, disabled: this.data.readOnly}, [Validators.required]),
      city_id: new FormControl({value: this.firmData.city_id, disabled: this.data.readOnly}, [Validators.required]),
      country_id: new FormControl({value: this.firmData.country_id, disabled: this.data.readOnly}, [Validators.required]),
    });

    const sub = this.eventService.subscribe('firm-status-update', (data) => {
      if(data.firmId === this.firmData.id && data.status === 1){
        this.form.get('name').enable();
        this.form.get('email').enable();
        this.form.get('phone').enable();
        this.form.get('city_id').enable();
        this.form.get('country_id').enable();
      }
    });
    this.subLIst.push(sub);

    this.errors = this.data.errors ? this.helperService.getListRecommendation(JSON.parse(this.data.errors), this.firmData.id) : [];
    this.recommendations = this.data.recomm ? this.helperService.getListRecommendation(JSON.parse(this.data.recomm), this.firmData.id) : [];

    const q = queue({concurrency: 1});
    if (!this.data.country || !this.data.country.length) {
      q.push(next => {
        const sub3 = this.dataService.getCountry()
          .subscribe((country) => {
            if (country.data && country.data.count > 0) {
              this.listCountry = country.data.items;
              this.cb.emit({
                country: this.listCountry,
              });
              next();
            }
          }, (error) => {
            this.toasterService.pop('error', this.translates.NETWORK_ERROR);
            next();
          });

        this.subLIst.push(sub3);
      });
    } else {
      this.listCountry = this.data.country;
    }

    if(!this.data.city || !this.data.city.length) {
      q.push(next => {
        if (this.firmData && this.firmData.country_id) {
          const sub3 = this.dataService.getCity(this.firmData.country_id)
            .subscribe((res) => {
              if (res.data && res.data.count > 0) {
                this.listCity = res.data.items;
                this.cb.emit({
                  city: this.listCity,
                });
                next();
              }
            }, (error) => {
              this.toasterService.pop('error', this.translates.NETWORK_ERROR);
              next();
            });

          this.subLIst.push(sub3);
        } else {
          next();
        }
      });
    } else {
      this.listCity = this.data.city;
    }

    q.start((data) => {
      this.isLoaded = true;
    });
  }

  ngOnChanges(changes) {
    console.log('changes', changes);
    /*    this.form.get('name').disabled();
          email: new FormControl({value: this.firmData.email, disabled: this.data.readOnly}, [Validators.required, Validators.email]),
          phone: new FormControl({value: this.firmData.phone, disabled: this.data.readOnly}, [Validators.required]),
          city_id: new FormControl({value: this.firmData.city_id, disabled: this.data.readOnly}, [Validators.required]),
          country_id: new FormControl({value: this.firmData.country_id, disabled: this.data.readOnly}, [Validators.required]),
        });*/
  }

  ngAfterViewInit() {
    console.log('ngAfterViewInit');
  }

  ngAfterContentCheck() {
    console.log('ngAfterContentCheck');
  }

  changePublicStatusHandler() {
    this.changePublicStatus.emit();
  }

  changeCountry() {
    if (this.form.value.country_id) {
      this.isLoaded = false;
      const sub = this.dataService.getCity(this.form.value.country_id)
        .subscribe((res) => {
          this.listCity = res.data.items;
          this.cb.emit({
            city: this.listCity,
          });
          this.isLoaded = true;
        });
      this.subLIst.push(sub);
    }
  }

  onSubmit() {
    if (!this.form.invalid) {
      const {
        name,
        email,
        phone,
        country_id,
        city_id
      } = this.form.value;

      const data = {
        id: (this.firmData && this.firmData.id) || '',
        name,
        email,
        phone,
        country_id,
        city_id
      };

      this.isLoading = true;
      const sub = this.firmService.saveCommon(data)
        .subscribe((res) => {
          this.isLoading = false;
          if (res.status !== 200) {
            this.toasterService.pop('error', this.translates.VALIDATE_ERROR);
            if (res.error.list) {
              if (res.error.list['slug'] && res.error.list['slug'][0] === 'slug must be unique') {
                res.error.list['name'] = ['NAME_ALREADY_EXISTS'];
              }

              Object.keys(res.error.list).forEach((field) => {
                const el = this.form.get(field);
                if (el) {
                  el.setErrors({
                    [res.error.list[field]]: true
                  });
                }
              });
            }
          } else {
            this.toasterService.pop('success', this.translates.COMMON_DATA_SUCCESS_SAVE);
            this.router.navigate(['/system', 'firms', res.data.id, 'about']);
          }
        }, (error) => {
          this.isLoading = false;
          this.toasterService.pop('error', this.translates.ERROR_DATA);
          throw new Error(error);
        });
      this.subLIst.push(sub);
    } else {
      this.toasterService.pop('error', this.translates.VALIDATE_ERROR);
    }

  }

  ngOnDestroy() {
    if (this.subLIst.length) {
      this.subLIst.forEach((item) => {
        item.unsubscribe();
      })
    }
  }

}
