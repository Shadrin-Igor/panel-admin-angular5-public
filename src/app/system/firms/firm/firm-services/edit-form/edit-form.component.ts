import {Component, OnDestroy, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs/Subscription';
import {ToasterService} from 'angular2-toaster';
import * as queue from 'queue';

import config from '../../../../../config';
import {DataService} from '../../../../../shared/services/data.service';
import FirmServiceService from '../../../../../shared/services/firm-service.service';
import HelperService from '../../../../../shared/services/helper.service';
import FirmClientServiceService from '../../../../../shared/services/firm-client-service.service';
import {AuthService} from '../../../../../shared/services/auth.service';
import {FilesService} from '../../../../../shared/services/files.service';
import LangService from '../../../../../shared/services/lang.service';
import PublishService from '../../../../../shared/services/publish.service';

@Component({
  selector: 'wtp-edit-form.component',
  templateUrl: './edit-form.component.html'
})
export default class EditFormComponent implements OnInit, OnDestroy {

  modalContent = `Тут будет текст предисловая, говорящий о пользе добавленных услуг для компании`;
  translates = {
    FIRM_SERVICE_ADD_SUCCESS: 'Услуга успешно добавленна',
    FIRM_SERVICE_ADD_ERROR: 'Не удалось создат услугу',
    FIRM_SERVICE_UPDATE_SUCCESS: 'Услуга успешно сохранена',
    FIRM_SERVICE_UPDATE_ERROR: 'Не удалось сохранить услугу'
  };

  subs: Subscription[] = [];
  form: FormGroup;
  description: string = '';
  isLoading: boolean = false;
  isLoaded: boolean = false;
  firmId: number = 0;
  serviceId: number = 0;
  tokenKey: string = '';
  uploadUrl: string = '';
  images: string[] = [];
  tmpFiles: {}[] = [];
  errors: { title: string, description: string }[] = [];
  recomm: { title: string, description: string }[] = [];
  minDescriptionLength: number = 100;
  listFirmService: { id: number, name: string }[] = [];
  langNode: any;

  constructor(private activeModal: NgbActiveModal,
              private toasterService: ToasterService,
              private dataService: DataService,
              private helperService: HelperService,
              private firmClientServiceService: FirmClientServiceService,
              private authService: AuthService,
              private filesService: FilesService,
              private langService: LangService,
              private publishService: PublishService,
              private firmServiceService: FirmServiceService) {
  }

  closeModal() {
    this.activeModal.dismiss();
  }

  ngOnInit() {
    this.tokenKey = `JWT ${this.authService.getToken()}`;
    this.uploadUrl = `${config.baseUrl}/files/firms-service/${this.serviceId}`;

    this.form = new FormGroup({
      name: new FormControl(null, Validators.required),
      other: new FormControl(null),
      description: new FormControl(null, Validators.required)
    });

    const q = queue({concurrency: 1});
    q.push(next => {
      const sub = this.dataService.getFirmService()
        .subscribe(res => {
          this.listFirmService = res.items;
          next();
        });
      this.subs.push(sub);
    });

    if (this.serviceId) {
      q.push(next => {
        const sub = this.firmClientServiceService.getFirmUserService(this.firmId, this.serviceId)
          .subscribe(res => {
            const dataName = res.data.name;
            const checkRes = this.publishService.checkPageErrors(res.data, this.minDescriptionLength);
            this.errors = checkRes.errors;
            this.recomm = checkRes.recomm;

            const findRes = this.listFirmService.find(item => item.name === dataName);
            if (!findRes) {
              this.form.get('name').setValue('-1');
              this.form.get('other').setValue(dataName);
            } else {
              this.form.get('name').setValue(findRes.name);
            }
            if (res.data.images) {
              this.images = res.data.images;
            }

            this.form.get('description').setValue(res.data.description);
            this.description = res.data.description;
            next();
          });
        this.subs.push(sub);
      });
    }

    q.start(error => {
      this.isLoaded = true;
    });
  }

  onSubmit() {
    this.isLoading = true;
    if (!this.form.invalid && ( this.form.value.name !== '-1' || this.form.value.other )) {
      const data = {
        id: this.serviceId,
        name: this.form.value.name !== '-1' ? this.form.value.name : this.form.value.other,
        description: this.form.value.description,
        firm_id: this.firmId
      };

      const sub = this.firmServiceService.save(this.firmId, data)
        .subscribe((res: any) => {

          if (res.status === 200) {
            const newServiceId = res.data.id;
            this.toasterService.pop('success', !this.serviceId ? this.translates.FIRM_SERVICE_ADD_SUCCESS : this.translates.FIRM_SERVICE_UPDATE_SUCCESS);
            if (this.tmpFiles.length) {
              this.tmpFiles.forEach((item: any, index: number) => {
                const sub = this.filesService.update(item.id, {item_id: newServiceId})
                  .subscribe(res => {
                    // После сохранения всех картинок закрываем форму, так как при закрытии формы идет запрос на список позиций с картинками
                    if(index === this.tmpFiles.length -1 ){
                      this.isLoading = false;
                      this.activeModal.close();
                    }
                  });
                this.subs.push(sub);
              });
            } else {
              this.isLoading = false;
              this.activeModal.close();
            }
          } else {
            this.isLoading = false;
            this.toasterService.pop('success', !this.serviceId ? this.translates.FIRM_SERVICE_ADD_ERROR : this.translates.FIRM_SERVICE_UPDATE_ERROR);
          }
        });

      this.subs.push(sub);
    }
  }

  onChangeDescription(value) {
    if (!this.langNode) this.langNode = this.langService.getNode('firmPage');
    this.errors = this.errors.filter((item: any) => item.title !== this.langNode.errors.DESCRIPTION_EMPTY.title && item.title !== this.langNode.errors.DESCRIPTION_SMALL.title);
    if (value.length < this.minDescriptionLength) {
      if (value.length) {
        this.errors.push({
          title: this.langNode.errors.DESCRIPTION_SMALL.title,
          description: this.langNode.errors.DESCRIPTION_SMALL.description
        })
      } else {
        this.errors.push({
          title: this.langNode.errors.DESCRIPTION_EMPTY.title,
          description: this.langNode.errors.DESCRIPTION_EMPTY.description
        })
      }
    }
    this.form.get('description').setValue(value);
  }

  onUploadFinished(event) {
    if (event) {
      if (!this.serviceId) {
        this.tmpFiles.push({id: event.data.id});
      } else {
        if(!this.langNode)this.langNode = this.langService.getNode('firmPage');
        this.errors = this.errors.filter((item: any, index: number) => item.title !== this.langNode.errors.IMAGE_EMPTY.title);

        if (this.images.length < 3) {
          this.errors = this.errors.filter((item: any, index: number) => item.title !== this.langNode.recomm.IMAGE_LENGTH.title);
        }
      }
    }
  }

  ngOnDestroy() {
    this.helperService.unSubscribe(this.subs);
  }
}
