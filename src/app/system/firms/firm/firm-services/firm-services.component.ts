import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Params} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {ToasterService} from 'angular2-toaster';
import * as queue from 'queue';

import EditFormComponent from './edit-form/edit-form.component';
import FirmClientServiceService from '../../../../shared/services/firm-client-service.service';
import HelperService from '../../../../shared/services/helper.service';
import FirmServiceService from '../../../../shared/services/firm-service.service';

@Component({
  selector: 'wtp-firm-service',
  templateUrl: './firm-services.component.html'
})
export class FirmServicesComponent implements OnInit, OnDestroy {
  @Input() firmData: any;
  @Input() data: any;
  @Output() cb = new EventEmitter<any>();

  isLoaded: boolean = true;
  settings = {};
  source = [];
  subs: Subscription[] = [];
  listService = [];
  errors = [];
  recommendations = [];
  translates = {
    SERVICE_SUCCESS_DELETED: 'Услуга успешно удаленно',
  };

  constructor(private modalService: NgbModal,
              private firmService: FirmServiceService,
              private firmClientServiceService: FirmClientServiceService,
              private helperService: HelperService,
              private toasterService: ToasterService) {
  }

  ngOnInit() {
    this.settings = {
      noDataMessage: 'У вашей компании еще нет добавленны услуги',
      gallery: 'images',
      columns: [{
        field: 'id',
        title: 'ID',
        type: 'number',
        width: 50
      },
        {
          field: 'name',
          title: 'Название',
          type: 'string',
          filter: true,
          template: row => this.rowTemplate(row)
        }]
    };

    this.errors = this.data && this.data.errors ? this.helperService.getListRecommendation(JSON.parse(this.data.errors), this.firmData.id, 'services') : [];
    this.recommendations = this.data && this.data.recomm ? this.helperService.getListRecommendation(JSON.parse(this.data.recomm), this.firmData.id, 'services') : [];
  }

  rowTemplate(row: any): string {
    let cout = `<h3 class="color-green">${row.name}</h3><p>${row.description_small}</p>`;
    if(!row.is_valid)cout += '<div class="alert alert-danger"><strong>Не полная информация</strong></div>';
    return cout;
  }

  createService(serviceId: number) {
    const q = queue({concurrency: 1});

    const activeModal = this.modalService.open(EditFormComponent, {
      size: 'lg',
      backdrop: 'static',
      container: 'nb-layout',
    });
    activeModal.componentInstance.firmId = this.firmData.id;
    if (serviceId) activeModal.componentInstance.serviceId = serviceId;
    activeModal.result.then((result) => {
      this.isLoaded = false;
      q.push(next => {
        this.getList(() => {
          next();
        });
      });

      q.push(next => {
        this.firmValidate(next);
      });

      q.start(() => {
        this.isLoaded = true;
      });
    }, (reason) => {
      this.isLoaded = false;
      this.getList(() => {
        this.isLoaded = true;
      });
    });
  }

  getList(next) {
    const sub2 = this.firmClientServiceService.getFirmUserServices(this.firmData.id)
      .subscribe((res: any) => {
        if (res.status === 200) {
          this.listService = res.data.items.map(item => {
            item.description_small = this.helperService.stripTags(item.description, 200);
            return item;
          });
        }
        next();
      });

    this.subs.push(sub2);
  }

  onEdit(itemId) {
    this.createService(itemId);
  }

  onDeleteConfirm(id) {
    this.isLoaded = false;
    const sub = this.firmClientServiceService.deleteFirmUserService(this.firmData.id, id)
      .subscribe(res => {
        this.getList(() => {
          this.isLoaded = true;
          this.toasterService.pop('success', this.translates.SERVICE_SUCCESS_DELETED);

          this.firmValidate();
        });
      });
    this.subs.push(sub);
  }

  firmValidate(next = null) {
    const sub = this.firmService.get(`firms/${this.firmData.id}/validate`)
      .subscribe((res: any) => {
        this.errors = res.errors ? this.helperService.getListRecommendation(JSON.parse(res.errors), this.firmData.id, 'services') : [];
        this.recommendations = res.recomm ? this.helperService.getListRecommendation(JSON.parse(res.recomm), this.firmData.id, 'services') : [];
        if(next)next();
      });
    this.subs.push(sub);
  }

  ngOnDestroy() {
    this.helperService.unSubscribe(this.subs);
  }
}
