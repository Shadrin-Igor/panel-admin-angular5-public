import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import * as moment from 'moment';
import {ToasterService} from 'angular2-toaster';

import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FirmService} from '../../../shared/services/firm.service';
import HelperService from '../../../shared/services/helper.service';
import ConfirmComponent from '../../../shared/components/modals/confirm/confirm.component';
import {EventService} from '../../../shared/services/event.service';

@Component({
  selector: 'wtp-firm',
  templateUrl: './firm.component.html'
})
export class FirmComponent implements OnInit, OnDestroy {

  firmId: number = null;
  subs: Subscription[] = [];
  errorsMessages = [];
  recommMessage = [];
  activeTab = 'common';
  isLoaded: boolean = false;
  isOverLoaded: boolean = true;
  firmData: any = {};
  data: any = {readOnly: false};

  constructor(private toasterService: ToasterService,
              private route: ActivatedRoute,
              private firmService: FirmService,
              private modalService: NgbModal,
              private eventService: EventService,
              private helperService: HelperService) {
  }

  ngOnInit() {
    const sub = this.route.params
      .subscribe((params: Params) => {
        if (params['id']) {
          if (params['tab']) this.activeTab = params['tab'];
          this.firmId = +params['id'];
          const sub2 = this.firmService.getFirmAndAllEmbeds(this.firmId)
            .subscribe((res: any) => {
              this.firmData = res.data;

              if (this.firmData.status === 3 || (+this.firmData.status === 2 && res.data.moderation)) {
                this.setRecomm(res.data.moderation);
              }

              if (this.firmData.status === 3) {
                this.data.readOnly = true;
              }

              this.firmData.errors = res.data.errors ? this.helperService.getListRecommendation(JSON.parse(res.data.errors), this.firmId) : [];
              this.firmData.recommendations = res.data.recomm ? this.helperService.getListRecommendation(JSON.parse(res.data.recomm), this.firmId) : [];

              this.isLoaded = true;
            });
          this.subs.push(sub2);
        }
      });

    this.subs.push(sub);
  }


  setRecomm(moderation: any = {}) {
    if (this.firmData.status === 2 && moderation) {
      this.errorsMessages = [{
        title: 'Фирма отправленная на модерацию перед публикацией',
        description: `Тут будет текст описания<p>Дата передачи на модерацию: ${moment(moderation.date).format('DD.MM.YYYY')}</p><p>Время расмотрения: ${moment(moderation.date_limit).format('DD.MM.YYYY')}</p>`
      }];
    }

    if (this.firmData.status === 3) {
      this.recommMessage = [{
        title: 'Ваша фирма опубликованна',
        description: `Тут будет текст описания`
      }];
    }
  }

  modalChangeStatus() {
    const activeModal = this.modalService.open(ConfirmComponent, {
      container: 'nb-layout',
    });
    activeModal.componentInstance.buttonYes = 'Начать редактирование';
    activeModal.componentInstance.buttonNo = 'Отмена';
    activeModal.result.then((result) => {
      this.isOverLoaded = false;
      const sub = this.firmService.saveStatusNotPublish(this.firmId)
        .subscribe(data => {
          this.data.readOnly = false;
          this.isOverLoaded = true;
          this.eventService.broadcast('firm-status-update', {firmId: this.firmId, status: 1});
          this.toasterService.pop('success', 'Статус компанеии изминен, и теперь мы может вносить изминения');
        });
      this.subs.push(sub);
    }, (reason) => {
      // this.isLoaded = false;
    });
  }

  outData(dataIn) {
    this.setRecomm(dataIn.moderation);
    this.data = {...this.data, ...dataIn};
  }

  ngOnDestroy() {
    this.helperService.unSubscribe(this.subs);
  }
}
