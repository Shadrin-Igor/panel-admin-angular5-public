import {Component, Input, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import 'rxjs/add/operator/mergeMap';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ToasterService} from 'angular2-toaster';
import {ActivatedRoute, Router} from '@angular/router';
import * as queue from 'queue';

import config from '../../../../config';
import {FirmService} from '../../../../shared/services/firm.service';
import {AuthService} from '../../../../shared/services/auth.service';
import {FilesService} from '../../../../shared/services/files.service';
import HelperService from '../../../../shared/services/helper.service';

@Component({
  selector: 'wtp-firm-about',
  templateUrl: './firm-about.component.html'
})
export class FirmAboutComponent implements OnInit {
  @Input() firmData: any;

  isLoaded = false;
  isLoading = false;
  form: FormGroup;
  subLIst: Subscription[] = [];
  description: string = '';
  firmId: number = 0;
  tokenKey: string = '';
  uploadUrl: string = '';
  logo = [];
  errors = [];
  recommendations = [];

  translates = {
    'ERROR_DATA': 'Ошибка загруки',
    'COMMON_DATA_SUCCESS_SAVE': 'Информация о компании успешно сохранена',
    'VALIDATE_ERROR': 'Ошибка валидации',
    'IMAGE_SUCCESS_UPLOAD': 'Картинка успешно загруженна',
    'IMAGE_ERROR_UPLOAD': 'Не удалось загрузить фото'
  };

  constructor(private route: ActivatedRoute,
              private authService: AuthService,
              private filesService: FilesService,
              private firmService: FirmService,
              private router: Router,
              private helperService: HelperService,
              private toasterService: ToasterService) {
  }

  ngOnInit() {
    this.tokenKey = `JWT ${this.authService.getToken()}`;

    this.form = new FormGroup({
      description: new FormControl(this.firmData.description, [Validators.required])
    });

    this.errors = this.firmData.errors ? this.firmData.errors : [];
    this.recommendations = this.firmData.recomm ? this.firmData.recomm : [];
    this.uploadUrl = `${config.baseUrl}/files/firms/${this.firmData.id}/logo`;
    this.description = this.firmData.description;

    if (this.firmData.images && this.firmData.images.length) {
      this.logo.push(this.firmData.images[0]);
    }
  }

  onUploadFinished(responseData: any) {
    if (responseData.status === 200) {
      const q = queue({concurrency: 1});
      let validateData = {};
      if (responseData.data) {
        if (this.logo.length && responseData.data.id !== this.logo[0].id) {
          q.push(next => {
            const subsc = this.filesService.deleteFile(this.logo[0].id)
              .subscribe(() => {
                this.logo = [];
                next();
              });
            this.subLIst.push(subsc);
          });
        }

        q.push(next => {
          const sub = this.firmService.get(`firms/${this.firmData.id}/validate`)
            .subscribe((res: any) => {
              this.errors = res.errors ? this.helperService.getListRecommendation(JSON.parse(res.errors), this.firmData.id, 'about') : [];
              this.recommendations = res.recomm ? this.helperService.getListRecommendation(JSON.parse(res.recomm), this.firmData.id, 'about') : [];
              next();
            });
          this.subLIst.push(sub);
        });

        q.start(error => {
          if (!error) {
            if (responseData.data) {
              this.logo.push(responseData.data);
            }
            this.toasterService.pop('success', this.translates.IMAGE_SUCCESS_UPLOAD);
          }
        });

      }
    } else {
      this.toasterService.pop('error', this.translates.IMAGE_ERROR_UPLOAD);
    }
  }

  onChangeDescription(value) {
    this.form.get('description').setValue(value);
  }

  imageDelete() {
    const sub = this.firmService.get(`firms/${this.firmData.id}/validate`)
      .subscribe((res: any) => {
        this.errors = res.errors ? this.helperService.getListRecommendation(JSON.parse(res.errors), this.firmData.id, 'about') : [];
        this.recommendations = res.recomm ? this.helperService.getListRecommendation(JSON.parse(res.recomm), this.firmData.id, 'about') : [];
      });
    this.subLIst.push(sub);
  }

  onSubmit() {
    if (!this.form.invalid) {

      const data = {
        description: this.form.value.description,
        id: this.firmData.id
      };

      this.isLoading = true;
      const sub = this.firmService.put(`firms/${this.firmData.id}/about`, data)
        .subscribe((res) => {
          this.isLoading = false;
          if (res.status !== 200) {
            this.toasterService.pop('error', this.translates.VALIDATE_ERROR);
            if (res.error.list) {
              Object.keys(res.error.list).forEach((field) => {
                const el = this.form.get(field);
                el.setErrors({
                  [res.error.list[field]]: true
                });
              });
            }
          } else {
            this.toasterService.pop('success', this.translates.COMMON_DATA_SUCCESS_SAVE);
            this.errors = res.data.errors ? this.helperService.getListRecommendation(JSON.parse(res.data.errors), this.firmData.id, 'about') : [];
            this.recommendations = res.data.recomm ? this.helperService.getListRecommendation(JSON.parse(res.data.recomm), this.firmData.id, 'about') : [];
          }
        }, (error) => {
          this.isLoading = false;
          this.toasterService.pop('error', this.translates.ERROR_DATA);
          throw new Error(error);
        });
      this.subLIst.push(sub);
    } else {
      this.toasterService.pop('error', this.translates.VALIDATE_ERROR);
    }
  }

  back() {
    this.router.navigate(['system', 'firms', this.firmData.id]);
  }

  ngOnDestroy() {
    if (this.subLIst.length) {
      this.subLIst.forEach((item) => {
        item.unsubscribe();
      })
    }
  }

}
