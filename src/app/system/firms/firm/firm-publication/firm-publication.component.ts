import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import 'rxjs/add/operator/mergeMap';
import {ToasterService} from 'angular2-toaster';
import {ActivatedRoute} from '@angular/router';

import {FirmService} from '../../../../shared/services/firm.service';
import HelperService from '../../../../shared/services/helper.service';

@Component({
  selector: 'wtp-firm-publication',
  templateUrl: './firm-publication.component.html'
})
export class FirmPublicationComponent implements OnInit {
  @Input() firmData: any;
  @Input() data: any;
  @Output() cb = new EventEmitter<any>();

  isLoaded = true;
  isLoading = false;
  subs: Subscription[] = [];
  errors = [];
  recommendations = [];
  attentionText: string = '';
  settings: any = {};

  translates = {
    'FIRM_SUCCESS_SEND_TO_MODERATION': 'Информация о фирме отправленна на модерацию',
    'FIRM_ERROR_PUBLICATION': 'Фирма не может быть опубликованна',
    'FIRM_MODERATION_ATTENTION_TITLE': 'Порядок публикации',
    'FIRM_MODERATION_ATTENTION': 'Тут будет текст расказывающий о процедуре публикации. Мы отправляем вашу заявку на модерацию после чего Ваша фирма автоматически будет опубликованна. Если вы обновите информацию Вам нужно будет заново публиковать Вашу фирму.'
  };

  constructor(private route: ActivatedRoute,
              private firmService: FirmService,
              private helperService: HelperService,
              private toasterService: ToasterService) {
  }

  ngOnInit() {
    this.errors = this.data && this.data.errors ? this.helperService.getListRecommendation(JSON.parse(this.data.errors), this.firmData.id) : [];
    this.recommendations = this.data && this.data.recomm ? this.helperService.getListRecommendation(JSON.parse(this.data.recomm), this.firmData.id) : [];
  }

  publication() {
    this.errors = [];
    this.recommendations = [];
    this.isLoading = true;
    const sub = this.firmService.setPublish(this.firmData.id)
      .subscribe(res => {
        if ('recomm' in res) {
          this.recommendations = res.recomm ? this.helperService.getListRecommendation(JSON.parse(res.recomm), this.firmData.id) : [];
        }
        if (res.status === 200) {
          this.settings.errorsTitle = 'publish.attention';
          this.settings.activeIndex = 1;
          this.errors = [{
            title: this.translates.FIRM_MODERATION_ATTENTION_TITLE,
            description: this.translates.FIRM_MODERATION_ATTENTION,
            showButton: false
          }];
          this.toasterService.pop('success', this.translates.FIRM_SUCCESS_SEND_TO_MODERATION);
          if (res.moderation) {
            this.cb.emit(res.moderation);
          }
        } else {
          this.settings.errorsTitle = '';
          this.errors = res.errors ? this.helperService.getListRecommendation(JSON.parse(res.errors), this.firmData.id) : [];
          this.toasterService.pop('error', this.translates.FIRM_ERROR_PUBLICATION);
        }
        this.isLoading = false;
      });
    this.subs.push(sub);
  }

  ngOnDestroy() {
    this.helperService.unSubscribe(this.subs);
  }
}
