import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Params} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';

import EditFormComponent from './edit-form/edit-pages-form.component';
import FirmClientPageService from '../../../../shared/services/firm-client-page.service';
import HelperService from '../../../../shared/services/helper.service';
import {FirmService} from '../../../../shared/services/firm.service';

@Component({
  selector: 'wtp-firm-pages',
  templateUrl: './firm-pages.component.html',
  styleUrls: ['./firm-pages.component.scss']
})
export class FirmPagesComponent implements OnInit, OnDestroy {
  @Input() firmData: any;
  @Input() data: any;

  isLoaded: boolean = true;
  settings = {};
  subs: Subscription[] = [];
  errors = [];
  recommendations = [];
  listPage = [];
  translates = {
    PAGE_SUCCESS_DELETED: 'Страница успешно удаленна',
  };

  constructor(private modalService: NgbModal,
              private firmClientPageService: FirmClientPageService,
              private helperService: HelperService,
              private firmService: FirmService) {
  }

  ngOnInit() {
    this.errors = this.data && this.data.errors ? this.helperService.getListRecommendation(JSON.parse(this.data.errors), this.firmData.id, 'pages') : [];
    this.recommendations = this.data && this.data.recomm ? this.helperService.getListRecommendation(JSON.parse(this.data.recomm), this.firmData.id, 'pages') : [];

    this.settings = {
      noDataMessage: 'У вашей компании еще нет добавленны страниц',
      gallery: 'images',
      columns: [{
        field: 'id',
        title: 'ID',
        type: 'number',
        width: 50
      },
        {
          field: 'name',
          title: 'Название',
          type: 'string',
          filter: true,
          template: row => this.rowTemplate(row)
        }]
    };
  }

  rowTemplate(row: any): string {
    let cout = `<h3 class="color-green">${row.name}</h3><p>${row.description_small}</p>`;
    if(!row.is_valid)cout += '<div class="alert alert-danger"><strong>Не полная информация</strong></div>';
    return cout;
  }

  createPage(pageId: number) {
    const activeModal = this.modalService.open(EditFormComponent, {
      size: 'lg',
      backdrop: 'static',
      container: 'nb-layout',
    });
    activeModal.componentInstance.firmId = this.firmData.id;
    if (pageId) activeModal.componentInstance.pageId = pageId;
    activeModal.result.then((result) => {
      this.isLoaded = false;

      this.firmValidate(() => {
        this.isLoaded = true;
      });
    }, (reason) => {
      this.isLoaded = false;
      this.getList(() => {
        this.isLoaded = true;
      });
    });
  }

  getList(next) {
    const sub2 = this.firmClientPageService.getFirmUserPages(this.firmData.id)
      .subscribe((res: any) => {
        if (res.status === 200) {
          this.listPage = res.data.items.map(item => {
            item.description_small = this.helperService.stripTags(item.description, 200);
            return item;
          });
        }
        next();
      });

    this.subs.push(sub2);
  }

  onEdit(itemId: number = 0) {
    this.createPage(itemId);
  }

  firmValidate(next = null) {
    const sub = this.firmService.get(`firms/${this.firmData.id}/validate`)
      .subscribe((res: any) => {
        this.errors = res.errors ? this.helperService.getListRecommendation(JSON.parse(res.errors), this.firmData.id, 'pages') : [];
        this.recommendations = res.recomm ? this.helperService.getListRecommendation(JSON.parse(res.recomm), this.firmData.id, 'pages') : [];
        if (next) next();
      });
    this.subs.push(sub);
  }

  ngOnDestroy() {
    this.helperService.unSubscribe(this.subs);
  }
}
