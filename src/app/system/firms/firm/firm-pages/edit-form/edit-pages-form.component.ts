import {Component, OnDestroy, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs/Subscription';
import {ToasterService} from 'angular2-toaster';
import * as queue from 'queue';

import config from '../../../../../config';
import {DataService} from '../../../../../shared/services/data.service';
import FirmPageService from '../../../../../shared/services/firm-page.service';
import HelperService from '../../../../../shared/services/helper.service';
import FirmClientPageService from '../../../../../shared/services/firm-client-page.service';
import {AuthService} from '../../../../../shared/services/auth.service';
import {FilesService} from '../../../../../shared/services/files.service';
import LangService from '../../../../../shared/services/lang.service';
import PublishService from '../../../../../shared/services/publish.service';

@Component({
  selector: 'wtp-edit-pages-form.component',
  templateUrl: './edit-pages-form.component.html'
})
export default class EditPagesFormComponent implements OnInit, OnDestroy {

  modalContent = `Тут будет текст предисловая, говорящий о пользе добавленных страниц для компании`;
  translates = {
    FIRM_PAGES_ADD_SUCCESS: 'Страница успешно добавленна',
    FIRM_PAGES_ADD_ERROR: 'Не удалось сохранить страницу',
    FIRM_PAGES_UPDATE_SUCCESS: 'Страница успешно сохранена',
    FIRM_PAGES_UPDATE_ERROR: 'Не удалось обновить страницу'
  };

  subs: Subscription[] = [];
  form: FormGroup;
  description: string = '';
  isLoading: boolean = false;
  isLoaded: boolean = false;
  firmId: number = 0;
  pageId: number = 0;
  tokenKey: string = '';
  uploadUrl: string = '';
  images: string[] = [];
  tmpFiles: {}[] = [];
  newItemSlug = '';
  errors: { title: string, description: string }[] = [];
  recomm: { title: string, description: string }[] = [];
  minDescriptionLength: number = 100;
  listFirmPages: { id: number, name: string, slug: string }[] = [];
  langNode: any;

  constructor(private activeModal: NgbActiveModal,
              private toasterService: ToasterService,
              private dataService: DataService,
              private helperService: HelperService,
              private firmClientPageService: FirmClientPageService,
              private authService: AuthService,
              private filesService: FilesService,
              private langService: LangService,
              private publishService: PublishService,
              private firmPageService: FirmPageService) {
  }

  closeModal() {
    this.activeModal.dismiss();
  }

  ngOnInit() {
    this.tokenKey = `JWT ${this.authService.getToken()}`;
    this.uploadUrl = `${config.baseUrl}/files/firms-page/${this.pageId}`;

    this.form = new FormGroup({
      name: new FormControl(null, Validators.required),
      other: new FormControl(null),
      description: new FormControl(null, Validators.required)
    });

    const q = queue({concurrency: 1});
    q.push(next => {
      const sub = this.dataService.getFirmPages()
        .subscribe(res => {
          this.listFirmPages = res.items;
          next();
        });
      this.subs.push(sub);
    });

    if (this.pageId) {
      q.push(next => {
        const sub = this.firmClientPageService.getFirmUserPage(this.firmId, this.pageId)
          .subscribe(res => {
            const dataName = res.data.name;
            const checkRes = this.publishService.checkPageErrors(res.data, this.minDescriptionLength);
            this.errors = checkRes.errors;
            this.recomm = checkRes.recomm;

            const findRes = this.listFirmPages.find(item => item.name === dataName);
            if (!findRes) {
              this.form.get('name').setValue('-1');
              this.form.get('other').setValue(dataName);
            } else {
              this.form.get('name').setValue(findRes.name);
            }
            if (res.data.images) {
              this.images = res.data.images;
            }

            this.form.get('description').setValue(res.data.description);
            this.description = res.data.description;
            next();
          });
        this.subs.push(sub);
      });
    }

    q.start(error => {
      this.isLoaded = true;
    });
  }

  selectType(event: any) {
    const value = event.target.value;
    this.listFirmPages.forEach(item => {
      if (item.name === value) this.newItemSlug = item.slug;
    });
  }

  onSubmit() {
    this.isLoading = true;
    if (!this.form.invalid && ( this.form.value.name !== '-1' || this.form.value.other )) {
      const data: any = {
        id: this.pageId,
        name: this.form.value.name !== '-1' ? this.form.value.name : this.form.value.other,
        description: this.form.value.description,
        firm_id: this.firmId
      };
      if (this.newItemSlug) {
        data.slug = this.newItemSlug;
      }

      const sub = this.firmPageService.save(this.firmId, data)
        .subscribe((res: any) => {
          if (res.status === 200) {
            const newPagesId = res.data.id;
            this.toasterService.pop('success', !this.pageId ? this.translates.FIRM_PAGES_ADD_SUCCESS : this.translates.FIRM_PAGES_UPDATE_SUCCESS);

            if (this.tmpFiles.length) {
              this.tmpFiles.forEach((item: any, index: number) => {
                const sub = this.filesService.update(item.id, {item_id: newPagesId})
                  .subscribe(res => {
                    // После сохранения всех картинок закрываем форму, так как при закрытии формы идет запрос на список позиций с картинками
                    if (index === this.tmpFiles.length - 1) {
                      this.isLoading = false;
                      this.activeModal.close();
                    }
                  });
                this.subs.push(sub);
              });
            }
            else {
              this.isLoading = false;
              this.activeModal.close();
            }
          } else {
            this.isLoading = false;
            this.toasterService.pop('error', !this.pageId ? this.translates.FIRM_PAGES_ADD_ERROR : this.translates.FIRM_PAGES_UPDATE_ERROR);
          }
        });

      this.subs.push(sub);
    }
  }

  onChangeDescription(value) {
    if (!this.langNode) this.langNode = this.langService.getNode('firmPage');
    this.errors = this.errors.filter((item: any) => item.title !== this.langNode.errors.DESCRIPTION_EMPTY.title && item.title !== this.langNode.errors.DESCRIPTION_SMALL.title);
    if (value.length < this.minDescriptionLength) {
      if (value.length) {
        this.errors.push({
          title: this.langNode.errors.DESCRIPTION_SMALL.title,
          description: this.langNode.errors.DESCRIPTION_SMALL.description
        })
      } else {
        this.errors.push({
          title: this.langNode.errors.DESCRIPTION_EMPTY.title,
          description: this.langNode.errors.DESCRIPTION_EMPTY.description
        })
      }
    }
    this.form.get('description').setValue(value);
  }

  onUploadFinished(event) {
    if (event) {
      if (!this.pageId) {
        this.tmpFiles.push({id: event.data.id});
      } else {
        if (!this.langNode) this.langNode = this.langService.getNode('firmPage');
        this.errors = this.errors.filter((item: any, index: number) => item.title !== this.langNode.errors.IMAGE_EMPTY.title);

        if (this.images.length < 3) {
          this.errors = this.errors.filter((item: any, index: number) => item.title !== this.langNode.recomm.IMAGE_LENGTH.title);
        }
      }
    }
  }

  ngOnDestroy() {
    this.helperService.unSubscribe(this.subs);
  }
}
