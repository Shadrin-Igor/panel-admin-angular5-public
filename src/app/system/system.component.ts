import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {AuthService} from '../shared/services/auth.service';
import { MENU_ITEMS } from './system-menu';

@Component({
  selector: 'wtp-system',
  template:  '<ngx-sample-layout><nb-menu [items]="menu"></nb-menu><router-outlet></router-outlet></ngx-sample-layout>',
})
export class SystemComponent implements OnInit{
  menu = MENU_ITEMS;
  constructor(
    private authService: AuthService,
    private router: Router
  ) {  }

  ngOnInit() {
    if(!this.authService.isLoggedIn()){
      this.router.navigate(['/login']);
    }
  }
}
