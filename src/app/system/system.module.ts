import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ThemeModule} from '../@theme/theme.module';
import {DashboardComponent} from './dashboard/dashboard.component';
import {SystemRoutingModule} from './system-routing.module';
import {SharedModule} from '../shared/shared.module';
import {SystemComponent} from './system.component';
import {FirmsModule} from './firms/firms.module';
import {LaddaModule} from 'angular2-ladda';

@NgModule({
  imports: [
    CommonModule,
    ThemeModule,
    SharedModule,
    SystemRoutingModule,
    FirmsModule
  ],
  declarations: [
    SystemComponent,
    DashboardComponent,
  ],

  exports: [
    CommonModule,
    SharedModule
  ]
})
export class SystemModule {

}
