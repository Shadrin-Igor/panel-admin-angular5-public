import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {LoginComponent} from './login/login.component';
import {AuthComponent} from './auth.component';
import {LogOutComponent} from './log-out/log-out.component';
import {ConfirmComponent} from './confirm/confirm.component';

const routes: Routes = [
  {
    path: '', component: AuthComponent, children: [
    {path: 'login', component: LoginComponent},
    {path: 'logOut', component: LogOutComponent},
    {path: 'confirm', component: ConfirmComponent},
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class AuthRoutingModule {
}
