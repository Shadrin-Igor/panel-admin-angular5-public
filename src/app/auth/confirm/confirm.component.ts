import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../shared/services/auth.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs/Subscription';
import {UserService} from '../../shared/services/user.service';
import {ToasterService} from 'angular2-toaster';
import {ErrorService} from '../../shared/services/error.services/error.service';

@Component({
  selector: 'wtp-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {

  userName = '';
  userEmail = '';
  userAvatar = '';
  isLoading = false;
  private sub1: Subscription;

  constructor(private userService: UserService,
              private toasterService: ToasterService,
              private authService: AuthService,
              private errorService: ErrorService) {
  }

  form: FormGroup;
  translates = {
    'AUTH_FAILED': 'Во время авторизации произошла ошибка.',
    'AUTH_WRONG_PASSWORD': 'Не верный пароль'
  };

  ngOnInit() {
    const user = this.authService.getUser();
    this.userName = `${user.last_name} ${user.first_name}`;
    this.userEmail = user.email;
    this.userAvatar = this.userService.getUserAvatar(user.avatar, user.sex);

    this.form = new FormGroup({
      password: new FormControl(null, [Validators.required, this.userService.passwordMinValue.bind(this, 6)]),
    });
  }

  onSubmit() {
    const {password} = this.form.value;
    this.isLoading = true;
    this.sub1 = this.userService.checkAuth(this.userEmail, password)
      .subscribe((res: Response) => {
        if (res.status !== 200) {
          this.isLoading = false;
          this.toasterService.pop('error', this.translates.AUTH_WRONG_PASSWORD);
        } else {
          if (!this.authService.login(res)) {
            this.isLoading = false;
            this.toasterService.pop('error', this.translates.AUTH_FAILED);
          }
        }
      }, (error) => {
        this.isLoading = false;
        this.toasterService.pop('error', this.translates.AUTH_FAILED);
        // throw new Error(error);
      });
  }

  changeAccount() {
    this.authService.logout();
  }

  ngOnDestroy() {
    if (this.sub1)
      this.sub1.unsubscribe();
  }

}
