import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../shared/services/auth.service';

@Component({
  selector: 'wtp-auth',
  template: '<wtp-auth-layout><router-outlet></router-outlet></wtp-auth-layout>',
})
export class AuthComponent implements OnInit {

  constructor(private router: Router,
              private authService: AuthService) {
  }

  ngOnInit() {
    let redirect = '';
    if (this.authService.isLoggedIn()) redirect = '/system';
    else {
      redirect = this.authService.getToken() ? '/confirm' : '/login';
    }

    this.router.navigate([redirect]);
  }
}
