import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs/Subscription';

import {UserService} from '../../shared/services/user.service';
import {ToasterService} from 'angular2-toaster';
import {Router} from '@angular/router';
import {AuthService} from '../../shared/services/auth.service';

@Component({
  selector: 'wtp-app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit, OnDestroy {

  private sub1: Subscription;

  constructor(private userService: UserService,
              private toasterService: ToasterService,
              private authService: AuthService,
              private router: Router) {
  }

  form: FormGroup;
  translates = {
    'AUTH_FAILED': 'Во время авторизации произошла ошибка.',
    'AUTH_WRONG_EMAIL_OR_PASSWORD': 'Не верный логин или пароль'
  };

  ngOnInit() {

    if (this.authService.isLoggedIn()) this.router.navigate(['/system']);
    else if (this.authService.getToken()) {
      this.router.navigate(['/confirm']);
    }

    this.form = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required, this.userService.passwordMinValue.bind(this, 6)]),
    });
  }

  onSubmit() {
    const {email, password} = this.form.value;
    this.sub1 = this.userService.checkAuth(email, password)
      .subscribe((res: Response) => {
        if (res.status !== 200) {
          this.toasterService.pop('error', this.translates.AUTH_WRONG_EMAIL_OR_PASSWORD);
        } else {
          if (!this.authService.login(res)) {
            this.toasterService.pop('error', this.translates.AUTH_FAILED);
          }
        }
      }, (error) => {
        this.toasterService.pop('error', this.translates.AUTH_FAILED);
        throw new Error(error);
      });
  }

  ngOnDestroy() {
    if (this.sub1)
      this.sub1.unsubscribe();
  }
}
