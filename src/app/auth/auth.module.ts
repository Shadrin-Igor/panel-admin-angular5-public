import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {LoginComponent} from './login/login.component';
import {AuthRoutingModule} from './auth-routing.module';
import {AuthComponent} from './auth.component';
import {ThemeModule} from '../@theme/theme.module';
import {SharedModule} from '../shared/shared.module';
import { LogOutComponent } from './log-out/log-out.component';
import { ConfirmComponent } from './confirm/confirm.component';

@NgModule({
  declarations: [
    LoginComponent,
    AuthComponent,
    LogOutComponent,
    ConfirmComponent
  ],
  imports: [
    CommonModule,
    ThemeModule,
    AuthRoutingModule,
    SharedModule
  ],
  exports: [
    SharedModule
  ]
})
export class AuthModule {
}
