import publish from './publish';
import firmPage from './publish/firm.page';

export default {
  publish,
  firmPage
}
