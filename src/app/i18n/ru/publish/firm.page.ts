export default {
  'errors': {
    'DESCRIPTION_EMPTY': {
      title: 'Необходимо указать описания',
      description: 'Вам обязательно необходимо указать описания'
    },
    'DESCRIPTION_SMALL': {
      title: 'Тест описания слишком маленький',
      description: 'Вам необходимо указать большой текст'
    },
    'IMAGE_EMPTY': {
      title: 'Нобходимо выбрать картинку',
      description: 'Вам необходимо выбрать как миниму одну картинку'
    },
  },
  'recomm': {
    'IMAGE_LENGTH': {
      title: 'Мы рекомендуем загрузить 3 картинки',
      description: 'Мы рекомендуем загрузить 3 картинки'
    },
  }
}
