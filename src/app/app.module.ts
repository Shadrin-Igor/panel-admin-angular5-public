/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import {APP_BASE_HREF} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {CoreModule} from './@core/core.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {ToasterModule} from 'angular2-toaster';
import {ImageUploadModule} from "angular2-image-upload";
import {AppRoutingModule} from './app-routing.module';
import {ThemeModule} from './@theme/theme.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {LaddaModule} from 'angular2-ladda';

import {AppComponent} from './app.component';
import {AuthModule} from './auth/auth.module';
import {ErrorService} from './shared/services/error.services/error.service';
import {UserService} from './shared/services/user.service';
import {AuthService} from './shared/services/auth.service';
import {AuthInterceptor} from './shared/interceptor/auth.interceptor';
import {FirmService} from './shared/services/firm.service';
import {DataService} from './shared/services/data.service';
import {FilesService} from './shared/services/files.service';
import FirmServiceService from './shared/services/firm-service.service';
import FirmClientServiceService from './shared/services/firm-client-service.service';
import HelperService from './shared/services/helper.service';
import FirmPageService from './shared/services/firm-page.service';
import FirmClientPageService from './shared/services/firm-client-page.service';
import SmartTableService from './shared/services/smart-table.service';
import LangService from './shared/services/lang.service';
import PublishService from './shared/services/publish.service';
import {EventService} from './shared/services/event.service';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    AuthModule,
    ToasterModule,

    ImageUploadModule.forRoot(),
    LaddaModule.forRoot({style: "zoom-out"}),
    NgbModule.forRoot(),
    ThemeModule.forRoot(),
    CoreModule.forRoot(),

  ],
  bootstrap: [AppComponent],
  providers: [
    {provide: APP_BASE_HREF, useValue: '/'},
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    ErrorService,
    UserService,
    AuthService,
    FirmService,
    DataService,
    FilesService,
    FirmServiceService,
    FirmClientServiceService,
    FirmPageService,
    FirmClientPageService,
    HelperService,
    SmartTableService,
    LangService,
    PublishService,
    EventService
  ],
})
export class AppModule {
}
