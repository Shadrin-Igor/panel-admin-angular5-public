import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LaddaModule} from 'angular2-ladda';
import {CKEditorModule} from 'ng2-ckeditor';
import {ImageUploadModule} from 'angular2-image-upload';
import {SortablejsModule} from 'angular-sortablejs';
import {LightboxModule} from 'angular2-lightbox';
import {RouterModule} from '@angular/router';

import {ThemeModule} from '../@theme/theme.module';
import { FormInputErrorComponent } from './components/form-input-error/form-input-error.component';
import { PreloaderComponent } from './components/preloader/preloader.component';
import {WTPCKEditorComponent} from './components/ckeditor/ckeditor.component';
import {ImgUploadComponent} from './components/img-upload/img-upload.component';
import SmartTableComponent from './components/smart-table/smart-table.component';
import DeleteConfirmComponent from './components/delete-confirm/delete-confirm.component';
import { ImgGalleryComponent } from './components/img-gallery/img-gallery.component';
import { SecondMenuComponent } from './components/second-menu/second-menu.component';
import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import ListRecommendationsComponent from './components/list-recommendations/list-recommendations.component';
import ConfirmComponent from './components/modals/confirm/confirm.component';

@NgModule({
  imports: [
    ThemeModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    LaddaModule,
    CKEditorModule,
    ImageUploadModule,
    SortablejsModule,
    LightboxModule,
    RouterModule
  ],
  exports: [
    ThemeModule,
    ReactiveFormsModule,
    FormsModule,
    FormInputErrorComponent,
    WTPCKEditorComponent,
    PreloaderComponent,
    LaddaModule,
    ImageUploadModule,
    ImgUploadComponent,
    SmartTableComponent,
    SortablejsModule,
    ImgGalleryComponent,
    LightboxModule,
    SecondMenuComponent,
    CommonModule,
    BreadcrumbComponent,
    RouterModule,
    ListRecommendationsComponent
  ],
  declarations: [
    FormInputErrorComponent,
    WTPCKEditorComponent,
    PreloaderComponent,
    ImgUploadComponent,
    SmartTableComponent,
    DeleteConfirmComponent,
    ImgGalleryComponent,
    SecondMenuComponent,
    BreadcrumbComponent,
    PaginationComponent,
    ListRecommendationsComponent,
    ConfirmComponent
  ],
  entryComponents: [
    DeleteConfirmComponent,
    ConfirmComponent
  ]
})
export class SharedModule { }
