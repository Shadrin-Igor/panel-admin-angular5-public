import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Router} from '@angular/router';
import {catchError, finalize, map} from 'rxjs/operators';

import {AuthService} from '../services/auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService,
              private router: Router) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const userToken = this.authService.getToken();
    const request = req.clone({
      setHeaders: {
        Authorization: `JWT ${userToken}`
      }
    });

    return next.handle(request)
      .pipe(
        map(event => {
          if (event instanceof HttpResponse) {

            console.log('event', event);
            if (event.status === 401) {
              console.log('401');
              this.authService.disableAuth();
              return Observable.empty();
            }

            if (event.body.token_exp) {
              this.authService.refreshTokenExp(event.body.token_exp);
            }
          }
          return event;
        }),
        catchError(error => {
          return Observable.throw(error);
        }),
        finalize(() => {
        })
      );
  }
}
