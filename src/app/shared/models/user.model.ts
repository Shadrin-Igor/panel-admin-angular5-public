import {RightModel} from './right.model';

export class UserModel {
  constructor(
    public email: string,
    public password: string,
    public sex: boolean,
    public right: RightModel[],
    public role?: string,
    public last_name?: string,
    public avatar?: string,
    public first_name?: string,
    public id?: number,
  ) {}
}
