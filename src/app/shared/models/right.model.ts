export class RightModel {
  constructor(
    public create: boolean,
    public read: boolean,
    public update: boolean,
    public del: boolean,
    public block: string,
  ) {}
}
