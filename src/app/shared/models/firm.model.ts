export class FirmModel {
  constructor(
    name: string,
    country_id: string,
    city_id: string,
    email: string,
    phone: string,
    description: string,
    image: string,
    id?: number
  ) {}
}
