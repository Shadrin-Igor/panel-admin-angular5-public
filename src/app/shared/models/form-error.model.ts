export class FormErrorModel {
  constructor(
    public field: string,
    public text: string
  ) {}
}
