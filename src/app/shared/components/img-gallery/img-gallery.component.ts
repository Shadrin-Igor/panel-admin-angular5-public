import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'wtp-img-gallery',
  templateUrl: './img-gallery.component.html',
  styleUrls: ['./img-gallery.component.scss']
})
export class ImgGalleryComponent{
  @Input() list: string[] = [];
}
