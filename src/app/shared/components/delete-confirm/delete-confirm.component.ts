import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'wtp-delete-confirm.component',
  templateUrl: './delete-confirm.component.html'
})
export default class DeleteConfirmComponent{

  itemTitle: string = '';
  itemId: number = 0;
  constructor(private activeModal: NgbActiveModal) {
  }

  closeModal() {
    this.activeModal.dismiss();
  }

  deleteAction() {
    this.activeModal.close(this.itemId);
  }
}
