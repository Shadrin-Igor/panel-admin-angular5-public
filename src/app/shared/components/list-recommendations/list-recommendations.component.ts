import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

import DeleteConfirmComponent from '../delete-confirm/delete-confirm.component';
import {Subscription} from 'rxjs/Subscription';
import HelperService from '../../services/helper.service';
import {ToasterService} from 'angular2-toaster';
import SmartTableService from '../../services/smart-table.service';
import LangService from '../../services/lang.service';

@Component({
  selector: 'wtp-list-recommendations',
  templateUrl: './list-recommendations.component.html',
  styleUrls: ['./list-recommendations.component.scss']
})
export default class ListRecommendationsComponent implements OnInit {
  @Input() errors: {title: string, description: string, fixedLink: string, showButton: boolean}[];
  @Input() recomm: {title: string, description: string, fixedLink: string, showButton: boolean}[];
  @Input() settings: any;
  @Input() showButton: boolean = true;
  @Input() small: boolean = false;

  activePanel: string = '';
  titleError: string = '';
  titleRecomm: string = '';
  constructor(private langService: LangService) {
  }

  ngOnInit() {
    this.preparing();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.preparing();
  }

  preparing() {
    const titleError = this.settings && this.settings.errorsTitle ? this.settings.errorsTitle : 'publish.errorsTitle';
    const titleRecomm = this.settings && this.settings.recommTitle ? this.settings.recommTitle : 'publish.recommTitle';
    this.titleError = this.langService.getText(titleError);
    this.titleRecomm = this.langService.getText(titleRecomm);
    if(this.settings && this.settings.activeIndex) {
      this.activePanel = `error-${this.settings.activeIndex}`;
    }

    this.errors = this.errors.map((item: any) => {
      item.showButton = 'showButton' in item ? item.showButton : true;
      return item;
    });
  }
}
