import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'wtp-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

  @Input() count: number = 0;
  @Input() limit: number = 10;
  @Input() page: number = 1;
  @Output() onSelect = new EventEmitter<number>();

  listPage = [];

  ngOnInit() {
    const countPage = Math.ceil(this.count / this.limit);
    this.listPage = Array(countPage);
  }

  onClick(page: number) {
    if (this.page !== page) {
      this.page = page;
      this.onSelect.emit(page);
    }
  }
}
