import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'wtp-modal-confirm.component',
  templateUrl: './confirm.component.html'
})
export default class ConfirmComponent implements OnInit {

  modalContent = `Тут будет текст предисловая, говорящий о пользе добавленных страниц для компании`;

  constructor(private activeModal: NgbActiveModal) {
    console.log('ConfirmComponent');
  }

  closeModal() {
    this.activeModal.dismiss();
  }

  colvedModal() {
    this.activeModal.close();
  }

  ngOnInit() {
  }
}
