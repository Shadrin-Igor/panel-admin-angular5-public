import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Params, PRIMARY_OUTLET, Router} from '@angular/router';

interface IBreadcrumb {
  label: string;
  params: Params;
  url: string;
}

@Component({
  selector: 'wtp-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {

  breadcrumbs = [];

  constructor(private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    const root: ActivatedRoute = this.route.root;
    this.breadcrumbs = this.getBreadCrumbs(root);

    this.router.events
      .subscribe(event => {
        if (event => event instanceof NavigationEnd) {
          this.breadcrumbs = [];
          const root: ActivatedRoute = this.route.root;
          this.breadcrumbs = this.getBreadCrumbs(root);
        }
      });
  }

  getBreadCrumbs(route: ActivatedRoute, url: string = "", breadcrumbs: IBreadcrumb[] = []): IBreadcrumb[] {
    const ROUTE_DATA_BREADCRUMB: string = "breadcrumb";
    const children: ActivatedRoute[] = route.children;

    if (!children.length) {
      return this.breadcrumbs;
    }

    for (let child of children) {
      if (child.outlet !== PRIMARY_OUTLET) {
        continue;
      }

      const routeURL: string = child.snapshot.url.map(segment => segment.path).join('/');

      if (!child.snapshot.data.hasOwnProperty(ROUTE_DATA_BREADCRUMB) || !child.snapshot.data[ROUTE_DATA_BREADCRUMB]) {
        if(child.snapshot.data['breadcrumbAddUrl']){
          if (url[url.length - 1] !== '/') url += `/`;
          url += `${routeURL}`;
        }
        return this.getBreadCrumbs(child, url, this.breadcrumbs)
      }

      if (url[url.length - 1] !== '/') url += `/`;
      url += `${routeURL}`;

      const breadcrumb: IBreadcrumb = {
        label: child.snapshot.data[ROUTE_DATA_BREADCRUMB],
        params: child.snapshot.params,
        url
      };

      this.breadcrumbs.push(breadcrumb);

      return this.getBreadCrumbs(child, url, this.breadcrumbs);
    }
  }

}
