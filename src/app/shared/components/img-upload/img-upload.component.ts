import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {FilesService} from '../../services/files.service';
import {Subscription} from 'rxjs/Subscription';
import {ToasterService} from 'angular2-toaster';
import config from '../../../config';
import {defer} from 'underscore';
import HelperService from '../../services/helper.service';
import {Lightbox} from 'angular2-lightbox';

@Component({
  selector: 'img-upload',
  templateUrl: './img-upload.component.html',
  styleUrls: ['./img-upload.component.scss']
})
export class ImgUploadComponent implements OnInit, OnDestroy {
  @Input() max: number;
  @Input() url: number;
  @Input() uploadedFiles: string[] = [];
  @Output() uploadFinished = new EventEmitter<any>();
  @Output() onDelete = new EventEmitter<any>();

  translates = {
    'IMAGE_SUCCESS_DELETE': 'Картинка успешно удаленна',
    'IMAGE_ERROR_DELETE': 'Не удалось удалить файл',
    'IMAGE_SORT_SUCCESS_SAVE': 'Сортировка картинок успешно сохранена',
    'IMAGE_SORT_ERROR_SAVE': 'Не удалось сохранить новый порядок картинок',
    'ERROR_UPLOAD_IMAGE': 'Ошибика загрузки картинки',
    'ERROR_WRONG_FORMAT': 'Файл должен быть расширененим JPG или PNG, и размером не более 8mb '
  };
  subs: Subscription[] = [];
  tokenKey: string = '';
  showAddForm: boolean = false;
  isLoading: boolean = false;
  publicUrl: string = '';
  listUploadImages: string[] = [];
  options = {};
  album = [];

  constructor(private authService: AuthService,
              private fileService: FilesService,
              private helperService: HelperService,
              private lightboxService: Lightbox,
              private toasterService: ToasterService) {
    this.options = {
      onUpdate: (event: any) => {
        const imageSort = this.uploadedFiles.map((item: any, index: number) => ({id: [item.id], order: index}));
        const sub = this.fileService.saveOrder(imageSort)
          .subscribe(res => {
            if (res.status === 200) {
              this.toasterService.pop('success', this.translates.IMAGE_SORT_SUCCESS_SAVE);
            } else {
              this.toasterService.pop('error', this.translates.IMAGE_SORT_ERROR_SAVE);
            }
          });

        this.subs.push(sub);
      }
    };
  }

  ngOnInit() {
    this.tokenKey = `JWT ${this.authService.getToken()}`;
    this.publicUrl = config.publicUrl;

    this.album = this.uploadedFiles.map((item: any) => ({
      src: this.helperService.getImage(item),
      caption: '',
      thumb: this.helperService.getImage(item, 'thumb'),
    }));
  }

  preview(index: number) {
    this.lightboxService.open(this.album, index);
  }

  openAddForm() {
    this.showAddForm = !this.showAddForm;
  }

  onImageRemove(imgId: number) {
    this.isLoading = true;
    const sub = this.fileService.deleteFile(imgId)
      .subscribe(res => {
        this.isLoading = false;
        if (res.status === 200) {
          this.toasterService.pop('success', this.translates.IMAGE_SUCCESS_DELETE);
          this.uploadedFiles = this.uploadedFiles.filter((item: any) => item.id !== imgId);
          this.onDelete.emit();
        }
        else this.toasterService.pop('error', this.translates.IMAGE_ERROR_DELETE);
      });
    this.subs.push(sub);
  }

  onUploadFinished(event) {
    const responseData = JSON.parse(event.serverResponse._body);

    if (responseData.status === 200) {
      this.uploadedFiles.push(responseData.data);
      this.uploadFinished.emit(responseData);
    } else {
      this.toasterService.pop('error', this.translates.ERROR_UPLOAD_IMAGE, this.translates.ERROR_WRONG_FORMAT)
    }
    this.isLoading = false;
  }

  onRemoved(event) {
    this.isLoading = true;
    this.uploadedFiles.forEach((item: any) => {
      const sub = this.fileService.deleteFile(item.id)
        .subscribe(res => {
          if (res.status === 200) {
            this.uploadedFiles = [];
            this.onDelete.emit();
          }
          this.isLoading = false;
        });

      this.subs.push(sub);

      defer(() => {
        this.listUploadImages = [];
      })
    });
  }

  makeMain(id: number) {
    const sub = this.fileService.makeFileMain(id)
      .subscribe(res => {
        this.uploadedFiles = res.items;
      });
    this.subs.push(sub);
  }

  uploadStateChanged($event) {
    if ($event) this.isLoading = true;
  }

  ngOnDestroy() {
    this.helperService.unSubscribe(this.subs);
  }

}
