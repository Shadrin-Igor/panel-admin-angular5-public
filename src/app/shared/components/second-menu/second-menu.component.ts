import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'wtp-second-menu',
  templateUrl: './second-menu.component.html',
  styleUrls: ['./second-menu.component.scss']
})
export class SecondMenuComponent implements OnInit{

  @Input() items: {title: string, url: string, invisible?: boolean}[] = [];
  @Input() params: {} = {};
  @Output() click: EventEmitter<number> = new EventEmitter();

  url = '';
  constructor(
    private router : Router
  ) {
  }

  ngOnInit() {
    this.url = this.router.url;
    this.router.events.subscribe((event) => {
      if(this.url !== this.router.url) {
        this.url = this.router.url;
      }
    });
  }

  clickHandler(index) {
    this.click.emit(index);
  }

  goUrl(url: string, invisible: boolean) {
    if(!invisible) {
      this.router.navigate(url.split('/'));
    }
  }
}
