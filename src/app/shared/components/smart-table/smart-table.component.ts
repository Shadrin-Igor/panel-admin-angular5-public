import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

import DeleteConfirmComponent from '../delete-confirm/delete-confirm.component';
import {Subscription} from 'rxjs/Subscription';
import HelperService from '../../services/helper.service';
import {ToasterService} from 'angular2-toaster';
import SmartTableService from '../../services/smart-table.service';

@Component({
  selector: 'wtp-smart-table',
  templateUrl: './smart-table.component.html',
  styleUrls: ['./smart-table.component.scss']
})
export default class SmartTableComponent implements OnInit, OnDestroy {
  @Input() settings: any;
  @Input() sortable: boolean = false;
  @Input() sourceUrl: string = '';
  @Input() limit: number = 10;
  @Output() editAction = new EventEmitter<any>();
  @Output() deleteConfirm = new EventEmitter<any>();

  listColumn = 0;
  options = {};
  subs: Subscription[] = [];
  countAll: number = 0;
  page: number = 1;
  source = [];
  saveSortUrl = '';
  getAllUrl = '';
  translates = {
    'SORTING_SUCCESS_SAVE': 'Сортировка успешно сохраненна',
    'SORTING_ERROR_SAVE': 'Не удалось сохранить сортировку',
    'ITEM_SUCCESS_DELETE': 'Запись успешно удаленна',
  };
  isLoaded: boolean = false;

  constructor(private modalService: NgbModal,
              private helperService: HelperService,
              private smartTableService: SmartTableService,
              private toasterService: ToasterService) {
  }

  ngOnInit() {
    this.getDefaultSettings();
    this.listColumn = this.settings.columns.length + 2;
    this.getAllUrl = `${this.sourceUrl}?embed=images,statistic&limit=${this.limit}`;
    this.saveSortUrl = `${this.sourceUrl}/sort`;

    this.getList();

    this.options = {
      onUpdate: (event: any) => {
        const newSort = this.source.map((item: any, index: number) => ({id: [item.id], order: index + 1}));
        const sub = this.smartTableService.post(this.saveSortUrl, newSort)
          .subscribe(res => {
            if (res.status === 200) {
              this.toasterService.pop('success', this.translates.SORTING_SUCCESS_SAVE);
            } else {
              this.toasterService.pop('error', this.translates.SORTING_ERROR_SAVE);
            }
          });

        this.subs.push(sub);
      }
    };
    const galleryField = this.settings['gallery'];
    if (galleryField) {
      this.source.forEach((item) => {
        if (item[galleryField]) {
          item[galleryField] = item[galleryField].filter((image, index) => {
            if (index < 3) {
              image.fullLink = this.helperService.getImage(image, 'big');
              return image;
            }
          });
        }
      });
    }
  }

  getList() {
    const sub = this.smartTableService.get(this.getAllUrl)
      .subscribe((res: any) => {
        if (res.status === 200) {
          this.source = res.data.items.map(item => ({
            ...item,
            description_small: this.helperService.stripTags(item.description, 400),
          }));
          this.source.forEach(item => {
            if (item.images) {
              item.images = item.images.map(image => ({fullLink: this.helperService.getImage(image, 'big')}))
            }
          });
          this.countAll = res.data.count;
        }
        this.isLoaded = true;
      });
    this.subs.push(sub);
  }

  getDefaultSettings() {
    if (!this.settings || !this.settings.columns) {
      this.settings.columns = [{
        field: 'id',
        title: 'ID',
        type: 'number',
        width: 50
      },
        {
          field: 'name',
          title: 'Название',
          type: 'string',
          filter: true,
          template: (row) => `<h3 class="color-green">${row.name}</h3><p>${row.description_small}</p>`
        }
      ];
    }
  }

  deleteHandler(item) {
    const activeModal = this.modalService.open(DeleteConfirmComponent, {
      container: 'nb-layout',
    });
    activeModal.componentInstance.itemTitle = item.name;
    activeModal.componentInstance.itemId = item.id;
    activeModal.result.then((result) => {
      this.isLoaded = false;
      const sub = this.smartTableService.del(`${this.sourceUrl}`, item.id)
        .subscribe(res => {
          if (res.status === 200) {
            this.toasterService.pop('success', this.translates.ITEM_SUCCESS_DELETE);
            this.getList();
            if (this.deleteConfirm) {
              this.deleteConfirm.emit(result);
            }
          } else {
            this.isLoaded = true;
          }
        });
      this.subs.push(sub);

    }, (reason) => {
    });
  }

  onChangePage(page: number) {
    this.isLoaded = false;
    const sub = this.smartTableService.get(`${this.sourceUrl}?limit=${this.limit}&offset=${(page - 1) * this.limit}`)
      .subscribe((res: any) => {
        this.isLoaded = true;
        if (res.status === 200) {
          this.page = page;
          this.source = res.data.items;
        }
      });
    this.subs.push(sub);
  }

  ngOnDestroy() {
    this.helperService.unSubscribe(this.subs);
  }
}
