import {Component, EventEmitter, Input, Output} from '@angular/core';

import './ckeditor.loader';
import 'ckeditor';

@Component({
  selector: 'wtp-ckeditor',
  template: `
    <ckeditor
      [(ngModel)]="model"
      [config]="{extraPlugins: 'divarea', height: '320'}"
      [readonly]="false"
      (change)="changeDescription($event)"
      debounce="500">
    </ckeditor>`,
})
export class WTPCKEditorComponent {
  @Input() model;
  @Output() change = new EventEmitter<any>();

  changeDescription() {
    this.change.emit(this.model);
  }
}

/*
    (change)="onChange($event)"
    (ready)="onReady($event)"
    (focus)="onFocus($event)"
    (blur)="onBlur($event)"
 */
