import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {ErrorService} from '../../services/error.services/error.service';

@Component({
  selector: 'wtp-form-input-error',
  templateUrl: './form-input-error.component.html',
  styleUrls: ['./form-input-error.component.scss']
})
export class FormInputErrorComponent implements OnInit, OnChanges {
  @Input() errors = {};
  listErrors: String[] = [];

  constructor(private errorService: ErrorService) {
  }

  ngOnInit() {
    this.parseErrors();
  }

  ngOnChanges() {
    this.parseErrors();
  }

  private parseErrors() {
    let needError = true;
    this.listErrors = [];
    if (this.errors) {
      Object.keys(this.errors).forEach((key) => {
        if(needError) {
          let errorText = this.errorService.getErrorByKey(key);
          if (!errorText) errorText = key;
          this.listErrors.push(errorText);

          if (key === 'required') {
            needError = false;
          }
        }
      });
    }
  }
}
