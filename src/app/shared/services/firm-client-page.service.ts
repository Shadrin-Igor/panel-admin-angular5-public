import {Injectable} from '@angular/core';
import {BaseApi} from '../core/base-api';

@Injectable()
export default class FirmClientPageService extends BaseApi {
  public url = 'firm-user-pages';

  getFirmUserPage(firmId: number, id: number) {
    return this.get(`${this.url}/${firmId}/${id}?embed=images`);
  }

  getFirmUserPages(firmId: number) {
    return this.get(`${this.url}/${firmId}?embed=images`);
  }

  deleteFirmUserPage(firmId: number, id: number) {
    return this.del(`${this.url}/${firmId}`, id);
  }

  saveOrder(data: any) {
    return this.post(`${this.url}/sort`, data);
  }
}
