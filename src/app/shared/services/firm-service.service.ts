import {Injectable} from '@angular/core';
import {BaseApi} from '../core/base-api';

@Injectable()
export default class FirmServiceService extends BaseApi {
  save(firmID: number, data: any) {
    let action = '';
    let url = '';
    if(!data.id) {
      action = 'post';
      url = `firm-user-services/${firmID}`;
    } else {
      action = 'put';
      url = `firm-user-services/${firmID}/${data.id}`;
    }

    return this[action]( url, data);
  }
}
