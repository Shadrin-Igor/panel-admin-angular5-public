import {Injectable} from '@angular/core';
import {BaseApi} from '../core/base-api';

@Injectable()
export default class FirmClientServiceService extends BaseApi {
  public url = 'firm-user-services';

  getFirmUserService(firmId: number, id: number) {
    return this.get(`${this.url}/${firmId}/${id}?embed=images`);
  }

  getFirmUserServices(firmId: number) {
    return this.get(`${this.url}/${firmId}?embed=images`);
  }

  deleteFirmUserService(firmId: number, id: number) {
    return this.del(`${this.url}/${firmId}`, id);
  }

  saveOrder(data: any) {
    return this.post(`${this.url}/sort`, data);
  }
}
