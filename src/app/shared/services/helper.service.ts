import {Subscription} from 'rxjs/Subscription';
import config from '../../config';
import {Router} from '@angular/router';
import {Injectable} from '@angular/core';
import LangService from './lang.service';

@Injectable()
export default class HelperService {
  constructor(private router: Router,
              private langService: LangService) {
  }

  unSubscribe(list: Subscription[]) {
    list.forEach(item => {
      item.unsubscribe();
    });
  }

  getImage(fileObj: any, size: string = '') {
    return `${config.publicUrl}${fileObj.folder}${size ? size + '_' : ''}${fileObj.file}`;
  }

  stripTags(text: string, limit: number = 0) {
    const tmp = document.createElement('DIV');
    tmp.innerHTML = text;
    let clearText = tmp.textContent || tmp.innerText;
    if (limit && clearText.length > limit) {
      clearText = clearText.substr(0, limit);
      clearText += '...'
    }
    return clearText;
  }

  goUrl(url: string) {
    this.router.navigate([url]);
  }

  setLocalStore(key: string, data: any) {
    window.localStorage.setItem(`${config.LOCAL_DOP_TOKEN_KEY}-${key}`, JSON.stringify(data));
  }

  getLocalStore(key: string) {
    const data = window.localStorage.getItem(`${config.LOCAL_DOP_TOKEN_KEY}-${key}`);
    return data ? JSON.parse(data) : null;
  }

  public getListRecommendation(groups: any, firmId: number, group = '') {
    if (groups) {
      return !group ? this.getRecommendation(groups, firmId) : this.getListCategoryRecommendation(groups, firmId, group);
    }
    return [];
  }

  private getRecommendation(groups: any, firmId: number) {
    let list = [];
    Object.keys(groups).forEach((group: any) => {
      if (!groups) return;
      const data = this.langService.getNode(`publish.${group}`);
      if (groups[group] && groups[group].length) {
        list = list.concat(groups[group].map(item => {
          if (item)
            return {
              title: data[item.field] && data[item.field][item.error] ? data[item.field][item.error].title : `${item.field} ${item.error}`,
              description: data[item.field] && data[item.field][item.error] ? data[item.field][item.error].description : '',
              fixedLink: ['/system', 'firms', firmId, group]
            }
        }));
      }
    });
    return list;
  }

  private getListCategoryRecommendation(groups: any, firmId: number, group = '') {
    let list = [];
    const data = this.langService.getNode(`publish.${group}`);
    if (groups[group] && groups[group].length) {
      list = groups[group].map(item => ({
        title: data[item.field] && data[item.field][item.error] ? data[item.field][item.error].title : `${item.field} ${item.error}`,
        description: data[item.field] && data[item.field][item.error] ? data[item.field][item.error].description : '',
        fixedLink: ['/system', 'firms', firmId, group]
      }));
    }

    return list;
  }

}
