import {BaseApi} from '../core/base-api';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class FilesService extends BaseApi {
  constructor(public http: HttpClient) {
    super(http);
  }

  deleteFile(id) {
    return this.del(`files`, id);
  }

  getFiles(params: {section: string, item_id: number, type: string}) {
    return this.get(`files/${params.section}/${params.item_id}/${params.type}/`)
  }

  makeFileMain(id: number) {
    return this.get(`files/main/${id}`);
  }

  saveOrder(data: any) {
    return this.post(`files/sort`, data);
  }

  update(id, data) {
    return this.put(`files/${id}`, data);
  }
}
