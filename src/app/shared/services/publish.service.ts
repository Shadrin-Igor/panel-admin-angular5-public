import {Injectable} from '@angular/core';
import LangService from './lang.service';

@Injectable()
export default class PublishService {
  constructor(private langService: LangService) {}

  getError(group, field, error){
    switch (error) {
      case 'FIELD_MUST_FILLED':

        break;
    }
  }

  checkPageErrors(data: any, minDescriptionLength: number): {errors: Array<any>, recomm: Array<any>} {
    const errors = [];
    const recomm = [];
    if (!data.description ||
      data.description.length < minDescriptionLength ||
      !data.images ||
      !data.images.length
    ) {
      const lang = this.langService.getNode('firmPage');
      if (!data.description) {
        errors.push({
          title: lang.errors.DESCRIPTION_EMPTY.title,
          description: lang.errors.DESCRIPTION_EMPTY.description
        });
      }

      if (data.description && data.description.length < minDescriptionLength) {
        errors.push({
          title: lang.errors.DESCRIPTION_SMALL.title,
          description: lang.errors.DESCRIPTION_SMALL.description
        });
      }

      if (!data.images || !data.images.length) {
        errors.push({title: lang.errors.IMAGE_EMPTY.title, description: lang.errors.IMAGE_EMPTY.description});
      }

      if (data.images && data.images.length === 1) {
        recomm.push({title: lang.recomm.IMAGE_LENGTH.title, description: lang.recomm.IMAGE_LENGTH.description});
      }
    }

    return {errors, recomm}
  }

  deleteDescriptionErrors(errors: Array<any>): Array<any> {
    return [];
  }
}
