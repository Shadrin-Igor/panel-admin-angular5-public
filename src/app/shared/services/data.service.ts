import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {BaseApi} from '../core/base-api';

@Injectable()
export class DataService extends BaseApi {
  constructor(public http: HttpClient) {
    super(http);
  }

  public getCountry() {
    return this.get('data/countries');
  }

  public getCity(countryId: number) {
    return this.get(`data/cities/${countryId}`);
  }

  public getFirmService() {
    return this.get(`firm-services`);
  }

  public getFirmPages() {
    return this.get(`firm-pages`);
  }
}
