import {Injectable} from '@angular/core';
import {BaseApi} from '../core/base-api';

@Injectable()
export default class FirmPageService extends BaseApi {
  save(firmID: number, data: any) {
    let action = '';
    let url = '';
    if (!data.id) {
      action = 'post';
      url = `firm-user-pages/${firmID}`;
    } else {
      action = 'put';
      url = `firm-user-pages/${firmID}/${data.id}`;
    }

    return this[action](url, data);
  }
}
