import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {BaseApi} from '../core/base-api';

@Injectable()
export class FirmService extends BaseApi {
  apiKey = 'firms';

  constructor(public http: HttpClient) {
    super(http);
  }

  saveImage(data: any) {
    return this.put(`${this.apiKey}/${data.id}/image`, data);
  }

  saveCommon(data: any) {
    let action = '';
    let url = '';
    if (!data.id) {
      action = 'post';
      url = 'firms';
    }
    else {
      action = 'put';
      url = `${this.apiKey}/${data.id}/common`;
    }

    return this[action](url, data);
  }

  saveStatusNotPublish(firmId, status: number = 1) {
    return this.get(`firms/${firmId}/no-publish`);
  }

  getFirmAndAllEmbeds(firmId: number) {
    return this.get(`${this.apiKey}/${firmId}?embed=images, services, pages, country, city, recomm, moderation`);
  }

  getAll() {
    return this.get(this.apiKey);
  }

  delete(firmId: number) {
    return this.del(this.apiKey, firmId);
  }

  setPublish(firmId: number) {
    return this.get(`${this.apiKey}/${firmId}/publication`);
  }
}
