import i18n from '../../i18n/ru'

export default class LangService {
  texts = {};

  constructor() {
    this.texts = i18n;
  }

  getText(key: string) {
    if (this.texts[key]) {
      return this.texts[key];
    }

    const params = key.split('.');
    if (params.length > 1) {
      let textItem = this.texts;
      let text = '';
      params.forEach(param => {
        if (textItem[param]) {
          if (typeof textItem[param] === 'string') {
            text = textItem[param];
            return;
          }
          if (typeof textItem[param] === 'object') textItem = textItem[param];
        } else return null;
      });

      if (text) return text;
    }

    return null;
  }

  getNode(key: string) {
    if (typeof this.texts[key] === 'object') {
      return this.texts[key];
    }

    const params = key.split('.');
    if (params.length > 1) {
      let textItem = this.texts;
      let node = '';
      params.forEach((param, index) => {
        if (textItem[param]) {
          if (typeof textItem[param] === 'object') {
            if ((index + 1 ) === params.length) {
              node = textItem[param];
              return;
            }
            textItem = textItem[param];
          }
        } else return;
      });

      if (node) return node;
    }

    return null;
  }

}
