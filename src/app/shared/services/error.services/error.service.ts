import {Injectable} from '@angular/core';
import errors from './errors'

@Injectable()
export class ErrorService {
  getErrorByKey(key: string): string | null {
    return errors[key] ? errors[key] : null;
  }
}
