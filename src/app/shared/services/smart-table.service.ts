import {Injectable} from '@angular/core';
import {BaseApi} from '../core/base-api';
import {HttpClient} from '@angular/common/http';

@Injectable()
export default class SmartTableService extends BaseApi {
  constructor(
    public http: HttpClient
  ) {
    super(http);
  }
}
