import {Router} from '@angular/router';
import {Injectable} from '@angular/core';
import * as moment from 'moment';
import {UserModel} from '../models/user.model';

@Injectable()
export class AuthService {
  private isAuthenticated = false;
  private userData: UserModel;
  private token = '';
  private token_exp = 0;
  private timeOut;
  LOCAL_DOP_KEY = 'WTP';
  LOCAL_TOKEN_KEY = 'TOKEN';
  LOCAL_TOKEN_EXP_DATE_KEY = 'TOKEN_EXP';
  LOCAL_USER_KEY = 'USER';

  constructor(private router: Router) {
  }

  public isLoggedIn() {
    return this.isAuthenticated;
  }

  public getUser() {
    return this.userData;
  }

  public getToken() {
    return this.token;
  }

  public getTokenExp() {
    return this.token_exp;
  }

  private setStorage({token, token_exp, user}: any) {
    this.deleteStorage();
    window.localStorage.setItem(`${this.LOCAL_DOP_KEY}-${this.LOCAL_TOKEN_KEY}`, token);
    window.localStorage.setItem(`${this.LOCAL_DOP_KEY}-${this.LOCAL_TOKEN_EXP_DATE_KEY}`, token_exp.toString());
    window.localStorage.setItem(`${this.LOCAL_DOP_KEY}-${this.LOCAL_USER_KEY}`, JSON.stringify(user));
    this.token = token;
    this.token_exp = token_exp;
    this.userData = user;
  }

  public logout() {
    this.deleteStorage();
    this.router.navigate(['/login']);
  }

  public refreshTokenExp(tokenExp) {
    window.localStorage.removeItem(`${this.LOCAL_DOP_KEY}-${this.LOCAL_TOKEN_EXP_DATE_KEY}`);
    window.localStorage.setItem(`${this.LOCAL_DOP_KEY}-${this.LOCAL_TOKEN_EXP_DATE_KEY}`, tokenExp.toString());
  }

  public disableAuth() {
    this.isAuthenticated = false;
    this.router.navigate(['/login']);
  }

  public login(data: any): boolean {
    if (data.token && data.token_exp && data.user && data.user.id && data.user.email) {
      this.userData = data.user;
      this.setStorage({token: data.token, token_exp: data.token_exp, user: data.user});
      this.isAuthenticated = true;

      const finishTime = data.token_exp - moment().unix();
      this.timeOut = setTimeout(() => {
        console.log('Вышел по окончанию срока действия токена', data.token_exp, finishTime);
        this.disableAuth();
      }, finishTime * 1000);
      this.router.navigate(['/system']);
      return true;
    }

    return false;
  }

  public checkAuth() {
    this.getStorage();

    if (this.token && this.token_exp && this.userData && this.userData.id) {

      if (this.isValidTokenDate(this.token_exp)) {
        this.isAuthenticated = true;
        // this.router.navigate(['/system']);
      } else {
        this.router.navigate(['/login']);
      }
    } else {
      this.deleteStorage();
      this.router.navigate(['/login']);
    }
  }

  private deleteStorage() {
    this.isAuthenticated = false;
    window.localStorage.removeItem(`${this.LOCAL_DOP_KEY}-${this.LOCAL_TOKEN_KEY}`);
    window.localStorage.removeItem(`${this.LOCAL_DOP_KEY}-${this.LOCAL_TOKEN_EXP_DATE_KEY}`);
    window.localStorage.removeItem(`${this.LOCAL_DOP_KEY}-${this.LOCAL_USER_KEY}`);
    this.token = '';
  }

  private getStorage() {
    this.token = window.localStorage.getItem(`${this.LOCAL_DOP_KEY}-${this.LOCAL_TOKEN_KEY}`);
    this.token_exp = +window.localStorage.getItem(`${this.LOCAL_DOP_KEY}-${this.LOCAL_TOKEN_EXP_DATE_KEY}`);
    const userInfo = window.localStorage.getItem(`${this.LOCAL_DOP_KEY}-${this.LOCAL_USER_KEY}`);
    this.userData = JSON.parse(userInfo);
  }

  private isValidTokenDate(date: number) {
    return date > moment().unix();
  }
}
