import {BaseApi} from '../core/base-api';
import {FormControl} from '@angular/forms';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class UserService extends BaseApi {

  constructor(public  http: HttpClient,) {
    super(http);
  }

  checkAuth(email: string, password: string) {
    return this.post('auth', {email, password});
  }

  getUserAvatar(avatar, sex) {
    return avatar ? avatar : sex === 1 ? 'assets/images/avatarMan.svg' : 'assets/images/avatarWoman.svg';
  }

  passwordMinValue(min: number, control: FormControl) {
    return control.value && control.value.length < min ? {"passwordMinValue6": true} : null;
  }
}
