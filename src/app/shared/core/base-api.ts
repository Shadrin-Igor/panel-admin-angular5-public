import {Injectable, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import config from '../../config';
import * as url from 'url';

@Injectable()
export class BaseApi implements OnInit {
  private baseUrl = config.baseUrl;
  headers: HttpHeaders;

  constructor(public http: HttpClient) {
  }

  ngOnInit() {
    const headers = new HttpHeaders()
      .set("Content-Type", "application/json");
  }

  private getUrl(url: string): string {
    return `${this.baseUrl}${url}`;
  }

  public get(url: string): Observable<any> {
    return this.http.get(this.getUrl(url));
  }

  public post(url: string, data: object): Observable<any> {
    return this.http.post(this.getUrl(url), data, {headers: this.headers});
  }

  public put(url: string, data: any): Observable<any> {
    return this.http.put(this.getUrl(url), data, {headers: this.headers});
  }

  public del(url: string, id: number): Observable<any> {
    return this.http.delete(`${this.getUrl(url)}/${id}`, {headers: this.headers});
  }
}
