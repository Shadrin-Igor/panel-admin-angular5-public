/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from './@core/utils/analytics.service';
import {ToasterConfig} from 'angular2-toaster';
import {AuthService} from './shared/services/auth.service';

@Component({
  selector: 'wtp-app',
  template: '<toaster-container [toasterconfig]="toasterconfig"></toaster-container><router-outlet></router-outlet>',
})
export class AppComponent implements OnInit {

  constructor(
    private analytics: AnalyticsService,
    private authService: AuthService
  ) {
  }

  public toasterconfig : ToasterConfig =
    new ToasterConfig({
      showCloseButton: true,
      tapToDismiss: false,
      timeout: 5000,
      animation: 'flyRight'
    });

  ngOnInit(): void {
    this.analytics.trackPageViews();
    this.authService.checkAuth();
  }
}
