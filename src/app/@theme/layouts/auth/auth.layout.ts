import { Component } from '@angular/core';

// TODO: move layouts into the framework
@Component({
  selector: 'wtp-auth-layout',
  styleUrls: ['./wtp-auth-layout.scss'],
  template: `
    <nb-layout>
      <nb-layout-column>
        <nb-card>
          <nb-card-body>
            <ng-content select="router-outlet"></ng-content>
          </nb-card-body>
        </nb-card>
      </nb-layout-column>
    </nb-layout>`,
})
export class AuthLayoutComponent {
}
