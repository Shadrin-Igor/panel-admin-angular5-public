import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by"></span>
    <div class="pull-right">
      <span><a href="#">TrioGroup@ 2018</a></span>
    </div>
  `,
})
export class FooterComponent {
}
